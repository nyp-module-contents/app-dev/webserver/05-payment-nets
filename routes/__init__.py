"""
The routes module contains our routing configuration of our website.
You may do this in the main file but that will make it very messy and cluttered.
Ever time you create a new child route you just have to do the following

router = flask.Blueprint(<Your fileName without extension>, __name__, url_prefix="/<your preferred url group name>")
"""
import os

import flask
from werkzeug import utils
from data        import initialize_essential_data, initialize_test_data
from data.users  import UserRole
from utils.flask import logger
#	import the router from the child module, give it a name
#	else it binds to routes.debug which will cause circular import errors
#	don't ask why... its a python thing
import routes.debug    as RDebug
import routes.examples as RExample
import routes.auth     as RAuth
import routes.admin    as RAdmin
import routes.payment  as RPayment
#	Create an instance of the main router and serve it at '/', basically http://localhost:5000/
#	you can either define here in the initialization of blueprint or at register_blueprint (see below)
router = flask.Blueprint("routes", __name__, url_prefix="/")

#	Register child routers here
#	Register the blueprint (router) from our debug module, serve it at http://localhost:5000/debug
#	url_prefix has NO intellisense at this region, so you gotta memorize but the option is passed down
#	Think of routes as a Tree hierarchy, its easier to manage by giving them names at parent level
#	In case you want to shift them around (usually you don't)
#	Summary:
#		Just duplicate this line, change the blueprint, change the prefix. Simple :)
router.register_blueprint(RDebug.router,   url_prefix="/debug")
router.register_blueprint(RExample.router, url_prefix="/examples")
router.register_blueprint(RAuth.router,    url_prefix="/auth")
router.register_blueprint(RAdmin.router,   url_prefix="/admin")
router.register_blueprint(RPayment.router, url_prefix="/payment")

@router.before_app_first_request
def prepare_data_default():
	logger().info("Initializing essential data")
	initialize_essential_data()
	logger().info("Initializing test data")
	initialize_test_data()

@router.context_processor
def prepare_context() -> dict:
	logger().info("context processor")

	render_context = dict()
	render_context.setdefault("user",     None)
	render_context.setdefault("UserRole", UserRole)

	#	My current logged in user object
	render_context["user"] = flask.g.get("user")
	return render_context


#	Configure your routes of the router in this file
#		Some guess work	@router.get implies a serving a GET request
#		So a POST request implies @router.post
@router.get("/")
def page_home():
	"""
	This route renders the home page
	"""
	return flask.render_template("home.html")
	# return flask.abort(500)
 

@router.get("/cdn/<path:path>")
def serve_file(path: str):
	logger().info(f"Requesting to serve {path}")
	return flask.send_file(f"{os.getcwd()}/cdn/{path}")