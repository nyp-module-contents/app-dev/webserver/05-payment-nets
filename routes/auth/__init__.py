"""
The routes module contains stuff that are used for debugging.
Usually you don't include in production or deployment
"""
import flask
from   utils.flask          import logger
from   data.base.conditions import ConditionEQ
from   data.users           import Users, User, UserRole

#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("auth", __name__)

@router.before_app_request
def inject_user():
	logger().info("Before incoming request")
	if "userid" in flask.session:
		user = Users.retrieve(None, flask.session["userid"])
		assert (user is not None)
		flask.g.user = user

@router.get("/login")
def page_login():
	return flask.render_template("auth/login.html")


@router.get("/register")
def page_register():
	return flask.render_template("auth/register.html")


@router.get("/logout")
def api_logout():
	flask.session.clear()
	# if "token" in flask.session:
	#     flask.session.pop("token")
	return flask.redirect("/")


@router.get("/test")
def api_test():
	import json
	return json.dumps(list(flask.session.items()))

@router.post("/api/login")
def api_login():

	name     = flask.request.form.get("name",     None)
	password = flask.request.form.get("password", None)

	if not isinstance(name, str) or not isinstance(password, str):
		return flask.Response("Bad Request", 400)

	#	Attempt to login
	from data.users import Users

	user = Users.login(name, password)

	if user is None:
		return flask.Response("Unauthorized", 401)
	else:
		logger().info("Login Success")
		flask.session["token"]  = "somestuff"
		flask.session["userid"] = user.uuid
		flask.session["name"]   = user.name
		return flask.redirect("/")

@router.post("/api/register")
def api_register():
	try:
		name     = flask.request.form.get("name")
		password = flask.request.form.get("password")

		with Users.open() as handle:
			users    = Users.select(handle, ConditionEQ("name", name))
			if len(users) != 0:
				return flask.Response(response="This user name cannot be used", status=409)
			else:
				logger().info(f"{name} is unused. an account will be created.")
				Users.insert(handle, User(role=UserRole.USER, name=name, password=password))

		return flask.redirect("/auth/login")

	except Exception as e:
		logger().error(f"Error occurred {e}")
		return flask.abort(500)