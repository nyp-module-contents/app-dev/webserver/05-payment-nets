"""
The routes module contains stuff that are used for NETs QR payment
"""
import flask
import json
import os
import hashlib
import base64
import datetime
import requests
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("payment", __name__)

nets_api_key     = "311096c6-1292-45b2-92e8-7da090548233"
nets_api_skey    = "973f66e3-e592-4f01-b32e-665e6f8e2c58"
nets_api_server  = "https://0ruj5din0d.execute-api.ap-southeast-1.amazonaws.com/live"
# nets_api_server  = "https://uat-api.nets.com.sg:9065"
nets_api_gateway = {
	"request": f"{nets_api_server}/uat/merchantservices/qr/dynamic/v1/order/request",
	"query":   f"{nets_api_server}/uat/merchantservices/qr/dynamic/v1/transaction/query",
	"void":    f"{nets_api_server}/uat/merchantservices/qr/dynamic/v1/transaction/reversal"
}

def generate_signature(payload: dict):
	"""
	Generates a signature as depicted by NETS.
	"""
	content = json.dumps(payload) + nets_api_skey
	hash    = hashlib.sha256(content.encode("utf-8")).hexdigest().upper()
	sig     = base64.b64encode(bytes.fromhex(hash)).decode("utf-8")
	return sig

@router.get("/")
def payment():
	return flask.render_template("examples/payment.html")

@router.get("/request/<int:amount>")
def payment_request(amount: int):
	date   = datetime.datetime.now()
	stan   = 0
	
	#	TODO:	Make a config table to store this
	if stan >= 1000000:	#	0 ~ 999999
		stan = 1
	else:
		stan += 1

	payload = json.load(open(f"{os.getcwd()}/config/nets-qr-request.json", "r"))
	payload["amount"]           = str(amount)
	payload["npx_data"]["E201"] = payload["amount"]
	payload["stan"]             = f"{stan:06d}"
	payload["transaction_date"] = date.strftime("%m%d")		#	MMDD
	payload["transaction_time"] = date.strftime("%H%M%S")	#	HHmmss
	headers = {
		"Content-Type": "application/json",
		"KeyId":        nets_api_key,
		"Sign":         generate_signature(payload)
	}
	request = requests.request(
					timeout=2.0,
	 				method ="POST",
	 				url    =nets_api_gateway["request"],
	 				headers=headers,
	 				data   =json.dumps(payload))

	if request.ok:
		response = request.json()
		#	1	- Serve the data as shown below
		return flask.Response(json.dumps({
			"txn_identifier":   response["txn_identifier"],
			"amount":           response["amount"],
			"stan":             response["stan"],
			"transaction_date": response["transaction_date"],
			"transaction_time": response["transaction_time"],
			"qr_code":          response["qr_code"]
		}), status=200)
		#	2 ---- Render template
	else:
		return flask.Response(request.json(), status=request.status_code)

@router.post("/query")
def payment_query():
	payload = json.load(open(f"{os.getcwd()}/config/nets-qr-query.json", "r"))
	#	TODO:	Update payload
	content = flask.request.json
	payload["amount"]           = content["amount"]
	payload["stan"]             = content["stan"]
	payload["transaction_date"] = content["transaction_date"]
	payload["transaction_time"] = content["transaction_time"]
	payload["txn_identifier"]   = content["txn_identifier"]

	headers = {
		"Content-Type": "application/json",
		"KeyId":        nets_api_key,
		"Sign":         generate_signature(payload)
	}
	request = requests.request(
					timeout=2.0,
	 				method ="POST",
	 				url    =nets_api_gateway["query"],
	 				headers=headers,
	 				data   =json.dumps(payload))

	if (request.ok):
		return flask.Response(json.dumps(request.json()), status=request.status_code)
	else:
		return flask.Response(json.dumps(request.json()), status=request.status_code)


@router.post("/void")
def payment_void():
	payload = json.load(open(f"{os.getcwd()}/config/nets-qr-void.json", "r"))
	#	TODO:	Update payload
	#	TODO:	Update payload
	content = flask.request.json
	payload["amount"]           = content["amount"]
	payload["stan"]             = content["stan"]
	payload["transaction_date"] = content["transaction_date"]
	payload["transaction_time"] = content["transaction_time"]
	payload["txn_identifier"]   = content["txn_identifier"]

	headers = {
		"Content-Type": "application/json",
		"KeyId": nets_api_key,
		"Sign": generate_signature(payload)
	}
	request = requests.request(
		timeout=2.0,
		method="POST",
		url=nets_api_gateway["void"],
		headers=headers,
		data=json.dumps(payload))

	if (request.ok):
		return flask.Response(json.dumps(request.json()), status=request.status_code)
	else:
		return flask.Response(json.dumps(request.json()), status=request.status_code)