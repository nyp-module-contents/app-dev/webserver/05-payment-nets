"""
The routes module contains standalone examples
"""
import flask
from   utils.flask import logger
import routes.admin.products as RProducts
from data.users import UserRole, User
#	This is a child route, so we don't specify url_prefix like the previous one
router = flask.Blueprint("admin", __name__)
router.register_blueprint(RProducts.router, url_prefix="/products")

@router.before_request
def deny_not_admin():
	logger().info("Verify incoming request is administrator role")
	user: User = flask.g.get("user")
	if user is None:
		return flask.abort(401) #   Not Login
	else:
		#   if logged in but not admin
		if user.role != UserRole.ADMIN:
			return flask.abort(403)
		else:
			return None

@router.get("/")
def page_home():
	return flask.render_template("admin/dashboard.html")

