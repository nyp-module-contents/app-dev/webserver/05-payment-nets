import shelve
from datetime             import datetime
from uuid                 import uuid4
from typing import Generic, TypeVar, Dict, List
from data.base.conditions import Condition

class Row(object):
	def __init__(self, uuid: str = None):
		"""
		Constructor of a row in the table.

		Parameters
		----------
		uuid: str The unique identifier. Generates when None is specified
		"""
		self.__uuid         = str(uuid4()) if uuid is None else uuid
		self.__date_created = datetime.now()
		self.__date_updated = datetime.now()

	@property
	def uuid(self) -> str:
		"""
		Universally unique identifier
		"""
		return self.__uuid

	@property
	def date_created(self) -> datetime:
		"""
		The date time when this row is created
		"""
		return self.__date_created

	@property
	def date_updated(self) -> datetime:
		"""
		The date time when this row is updated
		"""
		return self.__date_updated

	def mark_updated(self):
		"""
		Update this rows's updated time stamp
		"""
		self.__date_updated = datetime.now()
		return self

	def to_json(self) -> dict:
		return dict(
			uuid       =self.uuid,
			dateUpdated=self.date_updated.isoformat(),
			dateCreated=self.date_created.isoformat(),
		)


T = TypeVar("T", Row, object)
T.__doc__ = "Generic type hint for Table"


class Table(Generic[T]):
	"""
	A generic table facade implementation. Simply inherit the table by specifying the Row Type
	"""
	@classmethod
	def open(cls) -> shelve.Shelf:
		return shelve.open("data", "c")

	@classmethod
	def get_table(cls, handle: shelve.Shelf = None) -> Dict[str, T]:
		"""
		Retrieves the table as a dictionary

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		"""
		ref         = handle if handle is not None else cls.open()
		table: dict = ref.get(cls.__name__, {})
		ref.close() if handle is None else None
		return table

	@classmethod
	def set_table(cls, handle: shelve.Shelf = None, table: Dict[str, T] = {}) -> Dict[str, T]:
		"""
		Updates the table

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		table: dict
			The table to replace in the persistent storage
		"""
		ref   = handle if handle is not None else cls.open()
		ref[cls.__name__] = table
		ref.close() if handle is None else None
		return table

	@classmethod
	def contains(cls, handle: shelve.Shelf = None, uid: str = None) -> bool:
		"""
		Checks if the specified id exists

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		uid: str
			The uuid to check for
		"""
		if uid is None:
			raise ValueError("") 
		else:
			return uid in cls.get_table(handle)

	@classmethod
	def retrieve(cls, handle: shelve.Shelf = None, uid: str = None) -> T:
		"""
		Retrieve one object represented by its uuid. Returns None if not found

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		uid: str
			The uuid to retrieve
		"""
		if not cls.contains(handle, uid):
			return None
		else:
			return cls.get_table(handle)[uid]


	@classmethod
	def select(cls, handle: shelve.Shelf = None, *conditions: Condition) -> List[T]:
		"""
		Select all rows fulfilling the conditions

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		conditions: Condition
			Specify N number of conditions to fulfill
		"""
		ref     = handle if handle is not None else cls.open()
		results = list(cls.get_table(ref).values())
		
		for condition in conditions:
			results = list(filter(lambda x: condition.verify(x), results))
		
		ref.close() if handle is None else None
		return results

	@classmethod
	def size(cls, handle: shelve.Shelf = None) -> int:
		"""
		Size of the table

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		"""
		ref    = handle if handle is not None else cls.open()
		length = len(cls.get_table(ref))
		ref.close() if handle is None else None
		return length

	@classmethod
	def clear(cls, handle: shelve.Shelf = None):
		"""
		Empty the table

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		"""
		ref   = handle if handle is not None else cls.open()
		table = cls.get_table(ref)
		table.clear()
		cls.set_table(ref, table)
		ref.close() if handle is None else None

	@classmethod
	def insert(cls, handle: shelve.Shelf = None, *targets: T):
		"""
		Insert N objects into the table

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		targets: Row
			N Number of rows to be inserted
		"""
		ref   = handle if handle is not None else cls.open()
		table = cls.get_table(ref)
		for t in targets:
			if t in table:
				ref.close() if handle is None else None
				raise KeyError(f"object : {t.uuid} {t} already exists in the table")
			else:
				table[t.uuid] = t

		cls.set_table(ref, table)
		ref.close() if handle is None else None
		return targets

	@classmethod
	def remove(cls, handle: shelve.Shelf = None, uid: str = None) -> T:
		"""
		Remove the specified row

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		uid: str
			The uuid of the row
		"""
		ref   = handle if handle is not None else cls.open()
		item  = None
		table = cls.get_table(ref)
		if uid in table:
			item = table[uid]
			del    table[uid]
		else:
			ref.close() if handle is None else None
			raise KeyError("The specified id doesn't exists")

		cls.set_table(ref, table)
		ref.close() if handle is None else None
		return item

	@classmethod
	def delete(cls, handle: shelve.Shelf = None, *conditions: Condition) -> int:
		"""
		Delete rows that fulfill the specified conditions

		Parameters
		----------
		handle: shelve.Shelf 
			Specify None to auto open and close else use existing handle
		conditions: Condition
			N Number of conditions to fulfill
		"""
		ref   = handle if handle is not None else cls.open()
		size  = cls.size()
		table = cls.get_table(ref)
		
		if not conditions:
			table.clear()
			cls.set_table(ref, table)
		else:
			shortlist = cls.select(ref, *conditions)
			size      = len(shortlist)
			for t in shortlist:
				del table[t.uuid]
			cls.set_table(ref, table)

		ref.close() if handle is None else None
		return size

	@classmethod
	def update(cls, handle: shelve.Shelf = None, *targets: T):
		"""
		Update the specified targets into the table

		Parameters
		----------
		handle:  shelve.Shelf 
			Specify None to auto open and close else use existing handle
		targets: Row 
			N number of rows to be updated
		"""
		ref   = handle if handle is not None else cls.open()
		table = cls.get_table(ref)
		for t in targets:
			if t.uuid in table:
				t.mark_updated()
				table[t.uuid] = t
			else:
				ref.close() if handle is None else None
				raise KeyError(f"object : {t.uuid} {t} don't exists in the table")
		cls.set_table(ref, table)
		
		ref.close() if handle is None else None

		return targets



# #	Example Usage
# if __name__ == "__main__":
# 	#	Declare your Object
# 	class Product(Row):
# 		def __init__(self, name = "", desc= ""):
# 			super().__init__(uuid=None)
# 			self.name = name
# 			self.desc = desc

# 	#	Declare a Table of your Object
# 	class Products(Table[Product]):
# 		#	No need to provide any extra implementation
# 		#	Unless otherwise you require
# 		pass


# 	#	Pattern 1: Open once, Close once
# 	#	Pros: Efficient
# 	#	Cons: Pass the handle every where
# 	with Products.open() as handle:
# 		Products.clear(handle)
# 		Products.insert(handle, Product("Hello World", "Maybe this"), Product("Product 1", "Something"), Product("Product 2", "something"))
# 		contents = Products.select(handle, Condition.EQ("name", "Hello World"), Condition.OR("desc", "Maybe this", "Maybe That"))
# 		for c in contents:
# 			print(c.uuid, c.name, c.desc)
# 		print(Products.size(handle))

# 	#	Pattern 2:	Open and Close immediately per operation
# 	#	Pros: Easy to write and spam
# 	#	Cons: Slow
# 	Products.clear()
# 	Products.insert(None, Product("Hello World", "Maybe this"), Product("Product 1", "Something"), Product("Product 2", "something"))
# 	contents = Products.select(None, Condition.OR("name", "Product 1", "Product 2"))	
# 	for c in contents:
# 		print(c.uuid, c.name, c.desc)
# 	print(Products.size(None))